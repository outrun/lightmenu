lightmenu provides a set of wrapper functions to build simple menus and dialog boxes in bash scripts, 
based on linux dialog(1) (http://hightek.org/dialog/).

Building a basic menu is simple: create a local bash array, fill it with parameters, call the menu function. Example:

```bash
#!/bin/bash

source /usr/lib/lightmenu.bash
export BACKTITLE="Lightmenu DEMO on $(hostname -f)"

demomenu() {
  local -a items
  append items Window "Command in window" programbox ls -al $HOME
  append items Screen "Command on screen" runcommand ls -al $HOME
  append items MSG    "A message"         demo_mbox
  append items Switch "A status toggle"   lightmenu switcher
  lightmenu --title Mymenu --cancel Quit --status "An example demo menu" --title2 "Custom status" items
}
lightmenu demomenu
```

Screenshot:

![Screenshot](img/demomenu.png)
