#!/bin/bash
#============================================================================
# Title       : lightmenu.bash
# Description : Menu system based on Dialog and bash
# Author      : Bart Sjerps <bart@outrun.nl>
# License     : GPLv3+
# Manual      : man 7 lightmenu
# ---------------------------------------------------------------------------

# Shorthands
programbox() { ldialog prgbox "$@" ; }
messagebox() { ldialog msgbox "$@" ; }

# Run a command or set of commands in a subshell
# after showing the command(s) in a title, wait for ENTER when done
runcommand() {
  local cmd="$*" rc=
  clear
  printf "%s\n" "$cmd"
  printf '_%.0s' $(seq 1 $(tput cols))
  bash -c "$cmd" ; rc=$?
  printf "press enter to continue: " && read _
  return $rc
}

#============================================================================
# add exactly 3 elements (items) to a given array
# usage: append <arrayname> item1 [item2] [more items]
# ---------------------------------------------------------------------------

append() {
  local var=$1      ; shift
  local p1="$1"     ; shift
  local p2="${1:-}" ; shift
  local p3="$*"
  eval "$var+=(\"$p1\" \"$p2\" \"$p3\")"
}

#============================================================================
# The main dialog wrapper function. Call with 
# ldialog <mbox|yesno|prgbox> [options] <message|commands>
# ldialog <menu|list|form|checklist> [options] <array>
# ldialog <formlist|listcheck|formcheck> [options] <array1> <array2>
# ---------------------------------------------------------------------------

ldialog() {
  local dtype=$1; shift                                        # dialog type
  local ref1= ref2=                                            # array parameter references
  local -a a1 a2 p1 p2                                         # a1/a2=parameter arrays, p1/p2=customized
  local -a dp                                                  # final dialog parameters
  local -i cols=$(tput cols) lines=24                          # Screen dimensions
  local -i height1= height2= y1=2 y2= margin= width=           # window sizings
  local -i size1= size2= formx=14 flen=35 ilen=0               # box/form sizings
  local notags= nocancel= nook= defaultno= help= extra= debug= # flags
  local title= title2= msg= cmd= status=                       # titles, commands & messages
  local clabel= oklabel= hlabel= xlabel=                       # Button labels
  local hline="\\Z1CTRL-C\\Zn-Quit \\Z1ESC\\Zn-Back"           # help line window1
  local backtitle=${BACKTITLE:-Lightmenu on $(hostname)}       # Backtitle provided via env var
  local OPT OPTIND
  while getopts ":-:f:" OPT; do
    case "$OPT" in
      f) case $OPTARG in                     # flags, -f<flag>
           debug)     debug=1 ;;             # Debug output
           resize)    lines=$(tput lines) ;; # maximize the window vertically
           notags)    notags=1 ;;            # don't show TAGs
           nocancel)  nocancel=1 ;;          # No CANCEL button
           nook)      nook=1 ;;              # No OK button
           defaultno) defaultno=1 ;;         # Pre-select CANCEL
           *)         echo unknown flag -f"$OPTARG" >&2 ; return 20 ;;
         esac ;;
      -) case "${OPTARG}" in # long options with parameters
           margin)  margin="${!OPTIND}";     ((OPTIND++)) ;; # left/right margin
           height1) height1="${!OPTIND}";    ((OPTIND++)) ;; # window 1 height
           height2) height2="${!OPTIND}";    ((OPTIND++)) ;; # window 2 height
           formx)   formx="${!OPTIND}";      ((OPTIND++)) ;; # form offset
           title)   title="${!OPTIND}" ;     ((OPTIND++)) ;; # window 1 title
           title2)  title2="${!OPTIND}" ;    ((OPTIND++)) ;; # window 2 title
           status)  status="${!OPTIND}" ;    ((OPTIND++)) ;; # status message
           hline)   hline="${!OPTIND}" ;     ((OPTIND++)) ;; # help line (window 1)
           cancel)  clabel="${!OPTIND}";     ((OPTIND++)) ;; # change CANCEL label
           ok)      oklabel="${!OPTIND}";    ((OPTIND++)) ;; # chance OK label
           extra)   xlabel="${!OPTIND}";     ((OPTIND++)) ;; # change EXTRA label
           help)    hlabel="${!OPTIND}";     ((OPTIND++)) ;; # chance HELP label
           *)       echo  unknown option --"$OPTARG" >&2 ; return 20 ;;
         esac ;;
      *) echo unknown option "$OPTARG" >&2 ; return 20 ;;
    esac
  done
  shift $((OPTIND - 1))
  
  # Set some default parameters
  case $dtype in
    prgbox)       cmd="$*"
                  margin=$((margin?margin:2)) # make window as wide as possible
                  lines=$(tput lines) ;;      # and as high as possible
    yesno|msgbox) msg="$*" ;;                           # Set the display message
    *)            (($# > 0)) && ref1="${1}[@]" ; shift    # hack to load array1 and array2
                  (($# > 0)) && ref2="${1}[@]" ; shift ;; # into a1 and a2
  esac || true

  # Copy array with names ref1 & ref2 into arrays a1 and a2 (parameters)
  [[ -n $ref1 ]] && a1=("${!ref1}")
  [[ -n $ref2 ]] && a2=("${!ref2}")

  # Build customized parameter lists q1 and q2 depending on dialog type
  for (( i=0 ; i<${#a1[@]} ; i+=3 )) do
    [[ "${a1[i]}" == "help"  ]] && help=1 && continue   # HELP button
    [[ "${a1[i]}" == "extra" ]] && extra=1 && continue  # EXTRA button
    case $dtype in
      menu|list) p1+=("${a1[i]}" "${a1[i+1]}") ;; # only use 2 elements
      listcheck) p1+=("${a1[i]}" "${a1[i+1]}") ;;
      checklist) p1+=("${a1[i]}" "${a1[i+1]}" "${a1[i+2]}") ;; # 3 elements (3rd is on/off)
      # forms require special list of parameters
      form)      p1+=("${a1[i]}"  $((i/3+1)) 2 "${a1[i+1]}" $((i/3+1)) $formx $flen $ilen) ;;
      formlist)  p1+=("${a1[i]}"  $((i/3+1)) 2 "${a1[i+1]}" $((i/3+1)) $formx $flen $ilen) ;;
      formcheck) p1+=("${a1[i]}"  $((i/3+1)) 2 "${a1[i+1]}" $((i/3+1)) $formx $flen $ilen) ;;
    esac
  done
  for (( i=0 ; i<${#a2[@]} ; i+=3 )) do
    case $dtype in
      formlist)  p2+=("${a2[i]}" "${a2[i+1]}") ;;              # same as list
      formcheck) p2+=("${a2[i]}" "${a2[i+1]}" "${a2[i+2]}") ;; # same as checklist
      listcheck) p2+=("${a2[i]}" "${a2[i+1]}" "${a2[i+2]}") ;; # same as checklist
    esac
  done

  # Calculate window dimensions  
  case $dtype in
    formlist|formcheck|listcheck) height2=$((height2?height2:10)) ;; # increase height of 2nd window
    *)                            height2=$((height2?height2:4)) ;;
  esac
  
  height2=$((height1?lines - height1 - 5:height2)) # height1 has precedence
  margin=$((margin?margin:(cols - 74)/2))          # Default width = 74
  width=$((cols - 2*margin))                       # Width of all windows
  height1=$((lines - height2 - 5))                 # 5 lines are for borders, buttons etc
  y2=$((lines - height2 - 1))                      # topleft Y offset 2nd window
  size1=$((height1-6))                             # Inner box height 1st window
  size2=$((height2-6))                             # Inner box height 2nd window

  # Build status window
  [[ -n $status ]] && \
    dp+=(--colors --begin $y2 $margin --title "${title2:-Status}" 
         --infobox "$status" $height2 $width --and-widget)

  # Build first window
  dp+=(--colors --begin $y1 $margin --title "$title")
  case $dtype in
    yesno)         ;;
    prgbox|msgbox) hline="" ;;
    *)             ((${#p1[@]} == 0)) && echo "No parameters" >&2 && return 20 ;;
  esac
  case $dtype in
    menu)  export DIALOG_OK=6 ; cancel=${cancel:-Back} ;;
    yesno) ;;
    *)     export DIALOG_OK=0 ; nocancel=1 ;;
  esac
  [[ -n $hline     ]] && dp+=(--hline "$hline")
  [[ -n $clabel    ]] && dp+=(--cancel-label "$clabel")
  [[ -n $oklabel   ]] && dp+=(--ok-label "$oklabel")
  [[ -n $hlabel    ]] && dp+=(--help-label "$hlabel")
  [[ -n $xlabel    ]] && dp+=(--extra-label "$xlabel")
  [[ -n $help      ]] && dp+=(--help-button)
  [[ -n $extra     ]] && dp+=(--extra-button)
  [[ -n $notags    ]] && dp+=(--notags)
  [[ -n $nook      ]] && dp+=(--no-ok)
  [[ -n $defaultno ]] && dp+=(--defaultno)
  [[ -n $nocancel  ]] && dp+=(--no-cancel)
  case $dtype in
    msgbox)    dp+=(--msgbox  "$msg"        $height1 $width) ;;
    yesno)     dp+=(--yesno   "$msg"        $height1 $width) ;;
    prgbox)    dp+=(--prgbox  "$cmd" "$cmd" $height1 $width) ;;
    menu)      dp+=(--menu      "" $height1 $width $size1 "${p1[@]}") ;;
    list)      dp+=(--menu      "" $height1 $width $size1 "${p1[@]}") ;;
    form)      dp+=(--form      "" $height1 $width $size1 "${p1[@]}") ;;
    checklist) dp+=(--checklist "" $height1 $width $size1 "${p1[@]}") ;;
    formlist)  dp+=(--form      "" $height1 $width $size1 "${p1[@]}") ;;
    formcheck) dp+=(--form      "" $height1 $width $size1 "${p1[@]}") ;;
    listcheck) dp+=(--menu      "" $height1 $width $size1 "${p1[@]}") ;;
  esac

  # Build optional second window
  case $dtype in
    listcheck) dp+=(--and-widget --begin $y2 $margin --title "$title2" --separate-widget $'\n\t') ;;
    formlist)  dp+=(--and-widget --begin $y2 $margin --title "$title2" --separate-widget $'\t')   ;;
    formcheck) dp+=(--and-widget --begin $y2 $margin --title "$title2" --separate-widget $'\t')   ;;
  esac
  case $dtype in
    formlist)  dp+=(--menu      "" $height2 $width $size2 "${p2[@]}") ;;
    formcheck) dp+=(--checklist "" $height2 $width $size2 "${p2[@]}") ;;
    listcheck) dp+=(--checklist "" $height2 $width $size2 "${p2[@]}") ;;
  esac

  # Execute dialog
  export DIALOGRC=/usr/lib/lightmenu.rc
  export NCURSES_NO_UTF8_ACS=1 # UTF-8 fix for PuTTY
  export DIALOG_ESC=253        # Detect ESC button
  export DIALOG_CANCEL=5       # Change CANCEL return code
  
  export DIALOGOPTS="--no-mouse --keep-tite --no-collapse --cr-wrap --backtitle \"$backtitle\""
  [[ -n $debug ]] && printf "%s " "${dp[@]//--/$'\n'--}" $'\n' >&2 && sleep 1
  # /usr/bin/dialog "${dp[@]}" 3>"$(tty)" 2>&1 1>&3 # doesn't work with su - <user>
  /usr/bin/dialog "${dp[@]}" --output-fd 3 3>&1 1>&2
}

#============================================================================
# lightmenu <function>
# Keeps looping <function> until non-zero return code is received. Any return code not
# 5 (Cancel) or 253 (ESC) is considered an error.
# lightmenu [options] <array>
# runs 'ldialog menu <options> <array>' and executes the selected command
# in a subshell if rc=6 (OK), 2 (HELP) or 3 (EXTRA). 
# Cancel (5) or ESC (253) returns. Any other return code is an error and displays
# an error box.
# TBD: rc=130 = CTRL-C??
# ---------------------------------------------------------------------------
lightmenu() {
  if declare -F $1 &>/dev/null; then
    # called as function, start the loop
    local -r menucmd="$*"
    local rc=0
    while :; do
      $menucmd ; rc=$?
      case $rc in
        0) ;;
        5) return 0 ;;   # CANCEL
      253) return $rc ;; # ESC
        *) exit $rc ;;    
      esac
    done
  else
    # called from within the function, continue normal menu processing
    local ref="${!#}[@]" result= rc=0 cmd=
    local -a items=("${!ref}")
    result=$(ldialog menu "$@"); rc=$?
    case $rc in
        6) ;;              # OK, execute
        2) result=help ;;  # HELP, change cmd and execute
        3) result=extra ;; # EXTRA, change cmd and execute
        5) return $rc ;;   # Cancel/Quit
      253) return $rc ;;   # ESC
        *) echo "Unknown return code $rc" >&2; exit $rc ;;
    esac
    for (( i=0 ; i<${#items[@]} ; i+=3 )) do
      if [[ "$result" == "${items[i]}" ]]; then
        cmd="${items[i+2]}"
        ($cmd) ; rc=$?
        case $rc in
          0)   return 0 ;;   # No errors
          5)   return 0 ;;   # CANCEL
          130) return 0 ;;   # CTRL-C ?
          253) return $rc ;; # ESC
          *)   messagebox "\Z1$cmd\Zn\n\nreturned error $rc" ; return 0 ;;
        esac
      fi
    done
  fi
}
