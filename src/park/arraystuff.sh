
# Dump all elements of an array. $1=array name
dumparr() {
  local i
  for i in $(seq 0 $(( $(eval echo "\${#$1[@]}") - 1 ))); do 
    echo "$1[$i]="$(eval echo "\${$1[i]}")
  done
}

# Array 'has element' function. $1=array name $2=element
# returns 0 if $2 is in array, 1 if not
inarr() {
  local i
  for i in $(seq 0 $(( $(eval echo "\${#$1[@]}") - 1 ))); do
    [[ "$(eval echo "\${$1[i]}")" == "$2" ]] && return 0
  done
  return 1
}

# Find at which index the element can be found. $1 = array, $2 = element
array_index() {
  local i
  for i in $(seq 0 $(( $(eval echo "\${#$1[@]}") - 1 ))); do
    [[ "$(eval echo "\${$1[i]}")" == "$2" ]] && echo $i
  done
}
