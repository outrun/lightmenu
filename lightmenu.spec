Name:		lightmenu
Summary:	Dialog & bash based menu system
Version:	1.0.4.4
Release:	1%{?prerel:.~%prerel}
License:	GPLv3+
Group:		Outrun/Extras
Source0:	%{name}-%{version}.tbz2
BuildArch:	noarch
Requires:	dialog

%description
Lightweight ncurses dialog based menu system. Supports dynamic
menu entries, plugin menus and much more

%prep
%setup -q -n %{name}
%install
rm -rf %{buildroot}
install -m 0755 -d %{buildroot}/usr/lib
install -m 0755 -d %{buildroot}/usr/share/%{name}
install -m 0755 -d %{buildroot}/usr/share/doc/%{name}
install -m 0755 -d %{buildroot}/usr/share/man/man7/

cp -p lib/*   %{buildroot}/usr/lib
cp -p doc/*   %{buildroot}/usr/share/doc/%{name}
cp -p man7/*  %{buildroot}/usr/share/man/man7
cp -p share/* %{buildroot}/usr/share/%{name}


%files
%doc /usr/share/doc/lightmenu/*
%defattr(0755,root,root,755)
/usr/share/lightmenu
%defattr(0644,root,root)
/usr/lib/lightmenu.bash
/usr/lib/lightmenu.rc
/usr/share/man/man7/lightmenu.7.gz
